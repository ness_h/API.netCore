﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using food.API.Models;

namespace food.API
{
    public class FoodDataStore 
    {
        public static FoodDataStore Current { get; } = new FoodDataStore();
        public List<FoodDto> Food { get; set; }

        public FoodDataStore()
        {
            // init dummy data
            Food = new List<FoodDto>()
            {
                new FoodDto()
                {
                     Id = 1,
                     Name = "Pizza",
                     Description = "Pizza Marinara",
                     Components = new List<ComponentsDto>()
                     {
                         new ComponentsDto() {
                             Id = 1,
                             Name = "tomatoes" },
                          new ComponentsDto() {
                             Id = 2,
                             Name = "garlic"},
                          new ComponentsDto() {
                             Id = 2,
                             Name = "cheese"},
                     }
                },

                new FoodDto()
                {
                     Id = 1,
                     Name = "Burger",
                     Description = "Cheese Burger",
                     Components = new List<ComponentsDto>()
                     {
                         new ComponentsDto() {
                             Id = 1,
                             Name = "cheese" },
                          new ComponentsDto() {
                             Id = 2,
                             Name = "ketchup "},
                           new ComponentsDto() {
                             Id = 2,
                             Name = "meat "},
                     }
                },
                  new FoodDto()
                {
                     Id = 1,
                     Name = "Pasta",
                     Description = "",
                     Components = new List<ComponentsDto>()
                     {
                         new ComponentsDto() {
                             Id = 1,
                             Name = "Parmesan cheese" },
                          new ComponentsDto() {
                             Id = 2,
                             Name = "garlic"},
                     }
                }
            };

        }
    }
}
