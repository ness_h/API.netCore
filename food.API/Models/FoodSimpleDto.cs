﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace food.API.Models
{
    public class FoodSimpleDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
