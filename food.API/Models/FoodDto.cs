﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace food.API.Models
{
    public class FoodDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int NumberofComponents
        {
            get
            {
                return Components.Count;
            }
        }

        public ICollection<ComponentsDto> Components { get; set; }
        = new List<ComponentsDto>();
    }
}
