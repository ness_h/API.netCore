﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace food.API.Models
{
    public class ComponentsDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
