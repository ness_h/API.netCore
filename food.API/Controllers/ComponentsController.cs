﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using food.API.Models;
using food.API.Services;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace food.API.Controllers
{
    [Route("api/food")]
    public class ComponentsController : Controller
    {
        private ILogger<ComponentsController> _logger;
        private IMailService _mailService;
        private FoodInfoRepository _foodInfoRepository;


        public ComponentsController(ILogger<ComponentsController> logger, IMailService mailService)
        {
            _logger = logger;
            _mailService = mailService;
            _foodInfoRepository = FoodInfoRepository;
        }


        [HttpGet("{foodid}/components")]
        public IActionResult GetComponents(int foodid)
        {
            try
            {
                if (!_foodInfoRepository.FoodExists(foodid))
                {
                    _logger.LogInformation($"Food not found!");
                    return NotFound();
                }
                var componentsForFood = _foodInfoRepository.GetComponentsForFood(foodid);

                //var food = FoodDataStore.Current.Food.FirstOrDefault(c => c.Id == foodid);
                var componentsresult =
                                 Mapper.Map<IEnumerable<ComponentsDto>>(componentsForFood);

                return Ok(componentsresult);
            }
            catch (Exception ex)
            {
                _logger.LogCritical($"Exception found");
                return StatusCode(500, "A problem happened while handling your request.");
            }
        }

        [HttpGet("{foodid}/components/{id}", Name = "GetComponents")]
        public IActionResult GetComponents(int foodid, int id)
        {

            if (!_foodInfoRepository.FoodExists(foodid))
            {
                return NotFound();
            }

            var components = _foodInfoRepository.GetComponentsForFood(foodid, id);

            if (components == null)
            {
                return NotFound();
            }

            var componentsresult = Mapper.Map<ComponentsDto>(components);
            return Ok(componentsresult);
            
        }
        [HttpPost("{foodId}/components")]
        public IActionResult CreateComponents(int foodId,
         [FromBody] ComponentsForCreationDto Components)
        {
            if (Components == null)
            {
                return BadRequest();
            }


            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var food = FoodDataStore.Current.Food.FirstOrDefault(c => c.Id == foodId);

            if (food == null)
            {
                return NotFound();
            }

            var maxComponentsId = FoodDataStore.Current.Food.SelectMany(
                             c => c.Components).Max(p => p.Id);

            var finalComponents = new ComponentsDto()
            {
                Id = ++maxComponentsId,
                Name = Components.Name,
            };

            food.Components.Add(finalComponents);

            return CreatedAtRoute("GetComponents", new
            { foodId = foodId, id = finalComponents.Id }, finalComponents);
        }

        [HttpPut("{foodId}/components/{id}")]
        public IActionResult UpdateComponents(int foodId, int id,
            [FromBody] ComponentsForCreationDto Components)
        {
            if (Components == null)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var food = FoodDataStore.Current.Food.FirstOrDefault(c => c.Id == foodId);

            if (food == null)
            {
                return NotFound();
            }

            var ComponentsFromStore = food.Components.FirstOrDefault(p =>
            p.Id == id);

            if (ComponentsFromStore == null)
            {
                return NotFound();
            }

            ComponentsFromStore.Name = Components.Name;

            return NoContent();
        }


        [HttpPatch("{foodId}/components/{id}")]
        public IActionResult PartiallyUpdateComponents(int foodId, int id,
            [FromBody] JsonPatchDocument<ComponentsForCreationDto> patchDoc)
        {
            if (patchDoc == null)
            {
                return BadRequest();
            }

            var food = FoodDataStore.Current.Food.FirstOrDefault(c => c.Id == foodId);
            if (food == null)
            {
                return NotFound();
            }

            var ComponentsFromStore = food.Components.FirstOrDefault(c => c.Id == id);
            if (ComponentsFromStore == null)
            {
                return NotFound();
            }

            var ComponentsToPatch =
                   new ComponentsForCreationDto()
                   {
                       Name = ComponentsFromStore.Name,
                   };

            patchDoc.ApplyTo(ComponentsToPatch, ModelState);

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }


            TryValidateModel(ComponentsToPatch);

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ComponentsFromStore.Name = ComponentsToPatch.Name;

            return NoContent();
        }

        [HttpDelete("{foodId}/components/{id}")]
        public IActionResult DeleteComponents(int foodId, int id)
        {
            var food = FoodDataStore.Current.Food.FirstOrDefault(c => c.Id == foodId);
            if (food == null)
            {
                return NotFound();
            }

            var ComponentsFromStore = food.Components.FirstOrDefault(c => c.Id == id);
            if (ComponentsFromStore == null)
            {
                return NotFound();
            }

            food.Components.Remove(ComponentsFromStore);
            _mailService.Send("Point of interest deleted.","deleted");

            return NoContent();
        }
    }
}





