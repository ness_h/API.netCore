﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using food.API.Models;
using food.API.Services;
using Microsoft.AspNetCore.Mvc;

namespace food.API.Controllers
{
    [Route("api/food")]
    public class FoodController : Controller
    {
        private FoodInfoRepository _foodInfoRepository;

        [HttpGet()]
        public IActionResult GetFood()
        {
            // return Ok(FoodDataStore.Current.Food);
            var FoodEntities = _foodInfoRepository.GetFood();
           /* var results = new List<FoodSimpleDto>();
            foreach (var foodEntity in FoodEntities)
            {
                results.Add(new FoodSimpleDto
                {
                    Id = foodEntity.Id,
                    Description = foodEntity.Description,
                    Name = foodEntity.Name
                });
            }
            */

            var results = Mapper.Map<IEnumerable<FoodSimpleDto>>(FoodEntities);

            return Ok(results);
        }

        [HttpGet("{id}")]
        public IActionResult GetFood(int id, bool includeComponentns = false)
        {
            var food = _foodInfoRepository.GetFood(id, includeComponentns);

            if (food == null)
            {
                return NotFound();
            }
            if (includeComponentns)
            {
                var foodResult = Mapper.Map<FoodDto>(food);
                return Ok(foodResult);
            }

            var FoodWOComponents = Mapper.Map<FoodSimpleDto>(food);
            return Ok(FoodWOComponents);
            /*var chosenfood = FoodDataStore.Current.Food.FirstOrDefault(c => c.Id == id);
            if (chosenfood == null)
            {
                return NotFound();
            }

            return Ok(chosenfood);*/
        }
    }
}


