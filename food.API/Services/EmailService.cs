﻿namespace food.API.Services
{
    internal interface IMailService
    {
        void Send(string subject, string message);

    }
}