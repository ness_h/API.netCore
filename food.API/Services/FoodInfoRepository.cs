﻿using food.API.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace food.API.Services
{
    public class FoodInfoRepository : IFoodInfoRepository
    {
        private FoodInfo _context;
        public FoodInfoRepository(FoodInfo context)
        {
            _context = context;
        }
        public void AddComponentsForFood(int foodId, Components components)
        {
            var food = GetFood(foodId, false);
            food.Components.Add(components);
        }

        public bool FoodExists(int foodId)
        {
            return _context.Food.Any(c => c.Id == foodId);
        }

        public IEnumerable<FoodEntity> GetFood()
        {
            return _context.Food.OrderBy(c => c.Name).ToList();
        }
        public FoodEntity GetFood(int foodId, bool includeComponents)
        {
            if (includeComponents)
            {
                return _context.Food.Include(c => c.Components)
                    .Where(c => c.Id == foodId).FirstOrDefault();
            }
            return _context.Food.Where(c => c.Id == foodId).FirstOrDefault();     
        }
        public Components GetComponentsForFood (int foodId, int componentsId)
        {
            return _context.Components.Where(c => c.FoodId == foodId && c.Id == componentsId).FirstOrDefault();
        }
        public IEnumerable<Components> GetComponentsForFood(int foodId)
        {
            return _context.Components.Where(c => c.FoodId == foodId).ToList();
        }
        public void DeleteComponents(Components components)
        {
            _context.Components.Remove(components);
        }

        public bool Save()
        {
            return (_context.SaveChanges() >= 0);
        }
    }
}
