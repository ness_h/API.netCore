﻿using food.API.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace food.API.Services
{
    public interface IFoodInfoRepository
    {
        bool FoodExists(int foodId);
        IEnumerable<FoodEntity> GetFood();
        FoodEntity GetFood(int foodId, bool includeCompoennts);
        IEnumerable<Components> GetComponentsForFood(int foodId);
        Components GetComponentsForFood(int foodId, int componentsId);
        void AddComponentsForFood(int foodId, Components components);
        void DeleteComponents(Components components);
        bool Save();
    }
}



