﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace food.API.Entities
{
    public class FoodInfo : DbContext
    {
        public FoodInfo(DbContextOptions<FoodInfo> options)
           : base(options)
        {
            Database.Migrate();
        }

        public DbSet<FoodEntity> Food { get; set; }
        public DbSet<Components> Components { get; set; }

        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    optionsBuilder.UseSqlServer("connectionstring");

        //    base.OnConfiguring(optionsBuilder);
        //}
    }
}
