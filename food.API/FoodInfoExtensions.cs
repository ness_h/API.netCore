﻿using food.API.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace food.API
{
    public static class FoodInfoExtensions
    {
        public static void EnsureSeedDataForContext(this FoodInfo context)
        {
            if (context.Food.Any())
            {
                return;
            }

            // init seed data
            var foodlist = new List<FoodEntity>()
            {
                new FoodEntity()
                {
                     Name = "Pizza",
                     Description = "Pizza Marinara",
                     Components = new List<Components>()
                     {
                         new Components() {
                             Name = "tomatoes"
                         },
                          new Components() {
                           Name = "garlic"
                          },
                          new Components()
                          {
                              Name = "cheese"
                          },
                     }
                },
                new FoodEntity()
                {
                      Name = "Burger",
                     Description = "Cheese Burger",
                     Components = new List<Components>()
                     {
                         new Components() {
                            Name = "cheese"
                         },
                          new Components() {
                           Name = "ketchup "
                          },
                          new Components()
                          {
                              Name = "meat "
                          },
                     }
                },
                new FoodEntity()
                {
                     Name = "Pasta",
                     Description = "",
                     Components = new List<Components>()
                     {
                         new Components() {
                          Name = "Parmesan cheese"
                         },
                          new Components() {
                           Name = "garlic"
                          }
                     }
                }
            };

            context.Food.AddRange(foodlist);
            context.SaveChanges();
        }
    }
}
